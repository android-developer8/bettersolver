package com.bettersolver.interfaces

import com.bettersolver.model.ModelLogin
import org.json.JSONObject

interface LoginCallback {
    fun getlogin(jsonObject: JSONObject)
}
