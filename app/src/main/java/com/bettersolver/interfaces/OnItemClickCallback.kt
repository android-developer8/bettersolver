package com.bettersolver.interfaces

import android.view.View

interface OnItemClickCallback {
    fun onClickItem(view: View, position:Int)
}