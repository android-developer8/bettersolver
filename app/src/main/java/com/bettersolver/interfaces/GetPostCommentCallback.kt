package com.bettersolver.interfaces

import com.bettersolver.model.ModelGetComment
import org.json.JSONObject


interface GetPostCommentCallback {
    fun getAllPostComments(arrOfCategory: ArrayList<ModelGetComment>){

    }
    fun sendPostComments(jsonObject: JSONObject){

    }
}