package com.bettersolver.interfaces

import org.json.JSONObject

interface ChangePasswordCallback {
    fun getnewpassword(jsonObject: JSONObject)

}