package com.bettersolver.interfaces

import org.json.JSONObject

interface ForgotPasswordCallback {
    fun getsendtomail(jsonObject: JSONObject)

}