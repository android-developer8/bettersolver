package com.bettersolver.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bettersolver.R
import com.bettersolver.interfaces.OnItemClickCallback
import com.bettersolver.model.ModelGetComment

class PostCommentAdapter (var context: Context, var arrOfPostComment: ArrayList<ModelGetComment>,
                          var onItemClickCallback: OnItemClickCallback)
                            : RecyclerView.Adapter<PostCommentAdapter.PostCommentViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PostCommentViewHolder{
        val viewcomment = LayoutInflater.from(context).inflate(R.layout.cell_post_comments, null)
        return PostCommentViewHolder(viewcomment)
    }
    override fun getItemCount(): Int  {
        return arrOfPostComment.size
    }

    override fun onBindViewHolder(holder: PostCommentViewHolder, position: Int){
        val PostComment = arrOfPostComment [position]


        holder.tv_username.text = PostComment.username
        holder.tv_comment.text = PostComment.comment

        /*  Picasso.get()
              .load(modelCategory.category_image)
              .into(holder.categoryImp)*/

    }

    inner class PostCommentViewHolder(view: View) : RecyclerView.ViewHolder(view), View.OnClickListener {


        var tv_username: TextView = view.findViewById(R.id.c_username)
        var tv_comment: TextView = view.findViewById(R.id.tv_comments)
        var lytComment: LinearLayout = view.findViewById(R.id.lyt_postcomment)

        init {
            lytComment.setOnClickListener(this)
        }

        override fun onClick(view: View?) {
            val position:Int = view!!.tag as Int
            onItemClickCallback.onClickItem(view,position)
        }

    }

}


