package com.bettersolver.adapter

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.bettersolver.R
import com.bettersolver.activity.HomeFeedImgActivity
import com.bettersolver.activity.PostCommentActivity
import com.bettersolver.holder.PollViewHolder
import com.bettersolver.holder.PostViewHolder
import com.bettersolver.holder.QuestionViewHolder
import com.bettersolver.model.PostWallHome
import com.squareup.picasso.Picasso


class PostWallAdapter(private val postDataList: List<PostWallHome>) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val POST_TYPE_POST = 1
    private val POST_TYPE_QUESTION = 2
    private val POST_TYPE_POLL = 3


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val view: View


        return if (viewType == POST_TYPE_POST) {
            view = LayoutInflater.from(parent.context).inflate(R.layout.cell_post, parent, false)

            PostViewHolder(view) //object of PostViewHolder will return
        } else if (viewType == POST_TYPE_QUESTION) {
            view = LayoutInflater.from(parent.context).inflate(R.layout.cell_question, parent, false)

            QuestionViewHolder(view) //object of QuestionViewHolder will return
        } else {
            view = LayoutInflater.from(parent.context).inflate(R.layout.cell_poll, parent, false)

            PollViewHolder(view)
        }

    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {

        val postData = postDataList[position]
        val context = holder.itemView.context

        if (holder.itemViewType == POST_TYPE_POST && postData.type == "0") {
            val viewHolder = holder as PostViewHolder

            viewHolder.username.text = postData.username

            viewHolder.category.text = postData.category
            viewHolder.post_title.text = postData.post_title
            viewHolder.post_content.text = postData.post_content
            viewHolder.thanks.text = postData.thanks
            viewHolder.viewpoint.text = postData.viewpoint

            viewHolder.iv_cell_post.setOnClickListener {
                val intent = Intent(context, HomeFeedImgActivity::class.java)
                context.startActivity(intent)
                }

            viewHolder.viewpoint_img.setOnClickListener {
                val intent = Intent(context, PostCommentActivity::class.java)
                intent.putExtra("post_id",postData.id)
                context.startActivity(intent)
            }


            viewHolder.itemView.setOnClickListener {
                Toast.makeText(context, "POST type post", Toast.LENGTH_SHORT).show()
            }

        } else if (holder.itemViewType == POST_TYPE_QUESTION && postData.type == "1") {
            val viewHolder = holder as QuestionViewHolder

            viewHolder.username.text = postData.username
            if (postData.profileimage.isEmpty()) {
                Picasso.get()
                    .load(R.drawable.profile)
                    .resize(50, 50)
                    .centerCrop()
                    .into(viewHolder.profileimage)
            } else {
                Picasso.get()
                    .load(postData.profileimage)
                    .resize(50, 50)
                    .centerCrop()
                    .into(viewHolder.profileimage)
            }
            viewHolder.category.text = postData.category
            viewHolder.post_title.text = postData.post_title
            viewHolder.post_content.text = postData.post_content
            viewHolder.followers.text = postData.followers

            viewHolder.itemView.setOnClickListener {
                Toast.makeText(context, "Question type post", Toast.LENGTH_SHORT).show()
            }
        } else if (holder.itemViewType == POST_TYPE_POLL && postData.type == "2"){
            val viewHolder = holder as PollViewHolder

            viewHolder.username.text = postData.username
            if (postData.profileimage.isEmpty()) {
                Picasso.get().load(R.drawable.profile).resize(50, 50).centerCrop()
                    .into(viewHolder.profileimage)
            } else {
                Picasso.get().load(postData.profileimage).resize(50, 50).centerCrop()
                    .into(viewHolder.profileimage)
            }
            viewHolder.category.text = postData.category
            viewHolder.post_title.text = postData.post_title
            viewHolder.post_content.text = postData.post_content

            viewHolder.itemView.setOnClickListener {
                Toast.makeText(context, "Poll type post", Toast.LENGTH_SHORT).show()
            }
        }
    }




    override fun getItemCount(): Int {
        return postDataList.size
    }

    override fun getItemViewType(position: Int): Int {
        return if (postDataList[position].type == "0") {
            POST_TYPE_POST
        }else if(postDataList[position].type == "1"){
            POST_TYPE_QUESTION
        } else {
            POST_TYPE_POLL
        }

    }


}