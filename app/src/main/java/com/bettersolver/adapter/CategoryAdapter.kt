package com.bettersolver.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bettersolver.R
import com.bettersolver.interfaces.OnItemClickCallback
import com.bettersolver.model.Category
import de.hdodenhof.circleimageview.CircleImageView

class CategoryAdapter(var context: Context, var arrOfCategory: ArrayList<Category>, var categoryCallback: OnItemClickCallback) : RecyclerView.Adapter<CategoryAdapter.CategoryViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CategoryViewHolder{
        context = parent.context
        return CategoryViewHolder(LayoutInflater.from(context).inflate(R.layout.cell_category, parent, false))
    }
    override fun getItemCount(): Int  {
        return arrOfCategory.size
    }

    override fun onBindViewHolder(holder: CategoryViewHolder, position: Int){
        val modelCategory = arrOfCategory [position]


        holder.tvCateName.text = modelCategory.category_name

        /*  Picasso.get()
              .load(modelCategory.category_image)
              .into(holder.categoryImp)*/

    }

    inner class CategoryViewHolder(view: View) : RecyclerView.ViewHolder(view), View.OnClickListener {


        var categoryImp: CircleImageView = view.findViewById(R.id.categoryImg)
        var tvCateName: TextView = view.findViewById(R.id.tvCateName)
        var rlCategory: RelativeLayout = view.findViewById(R.id.rlCategory)

        init {
            rlCategory.setOnClickListener(this)
        }

        override fun onClick(view: View?) {
            val position:Int = view!!.tag as Int
            categoryCallback.onClickItem(view,position)
        }

    }

}