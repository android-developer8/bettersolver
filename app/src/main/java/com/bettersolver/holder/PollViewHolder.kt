package com.bettersolver.holder

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.cell_poll.view.*

class PollViewHolder(itemView: View?) : RecyclerView.ViewHolder(itemView!!) {
    val profileimage = itemView!!.profileimage
    val username = itemView!!.username
    val category = itemView!!.category
    val post_title = itemView!!.post_title
    val post_content = itemView!!.post_content
}
