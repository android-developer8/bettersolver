package com.bettersolver.holder

import android.content.Intent
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.cell_post.view.*

class PostViewHolder(itemView: View?) : RecyclerView.ViewHolder(itemView!!) {
    val profileimage = itemView!!.profileimage
    val username = itemView!!.username
    val category = itemView!!.category
    val post_title = itemView!!.post_title
    val post_content = itemView!!.post_content
    val thanks = itemView!!.thanks_tv
    val thanks_img = itemView!!.thanks_img
    val viewpoint_img = itemView!!.viewpoint_img
    val viewpoint = itemView!!.viewpoint_tv
    val share_img = itemView!!.share_img
    val iv_cell_post = itemView!!.iv_cell_post





}
