package com.bettersolver

import android.app.Application
import butterknife.ButterKnife

class AppControl : Application() {

    override fun onCreate() {
        super.onCreate()
        ConstantValue.BASEURL="http://demo.bettersolver.com/app_api.php?"
        ButterKnife.setDebug(true)
    }


}
