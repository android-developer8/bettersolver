package com.bettersolver

object ConstantKey {

    const val KEY_LOGIN_ID = "signin"
    const val KEY_DO = "do"
    const val KEY_REGISTER_ID = "signup"
    const val KEY_LOGOUT = "logout"
    const val KEY_FORGOTPASS = "forgotpassword"
    const val KEY_CHANGEPASS = "changepassword"
    const val KEY_POST_COMMENT = "postcomment"
    const val KEY_GET_COMMENT = "getcomment"
}
