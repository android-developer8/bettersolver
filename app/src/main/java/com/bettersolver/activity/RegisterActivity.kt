package com.bettersolver.activity

import android.app.DatePickerDialog
import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.bettersolver.ApiInterface
import com.bettersolver.ConstantKey
import com.bettersolver.R
import com.bettersolver.interfaces.SingupCallback
import com.bettersolver.model.ModelSingup
import com.bettersolver.utils.Utils
import kotlinx.android.synthetic.main.activity_register.*
import org.json.JSONObject
import java.util.*

class RegisterActivity : AppCompatActivity(), SingupCallback {

    lateinit var context:Context
    var gender = ""
    var dob = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)

        initAction();


    }

    private fun initAction() {


        radioGroup?.setOnCheckedChangeListener { group, checkedId ->

            if(R.id.radioMale == checkedId){
                gender = "0"
            }else if(R.id.radioFemale == checkedId){
                gender = "1"
            }

        }

        editTextDob.setOnClickListener {
            val calendar = Calendar.getInstance()
            val year = calendar.get(Calendar.YEAR)
            val month = calendar.get(Calendar.MONTH)
            val day = calendar.get(Calendar.DAY_OF_MONTH)

            val datePickerDialog =
                DatePickerDialog(this@RegisterActivity, DatePickerDialog.OnDateSetListener
                { view, year, monthOfYear, dayOfMonth ->

                    dob = "" + year + " - " + (monthOfYear + 1) + " - " + dayOfMonth
                    editTextDob.setText(dob)

                }, year, month, day)

            datePickerDialog.show()
        }


        txtAlreadyAc.setOnClickListener {
            startActivity(Intent(this@RegisterActivity, LoginActivity::class.java))
        }

        ivBackLogin.setOnClickListener {
            startActivity(Intent(this@RegisterActivity, LoginActivity::class.java))
        }

        cirRegisterButton.setOnClickListener {
            //if user pressed on button register
            //here we will register the user to server
            registerUser()
        }


    }

    private fun registerUser() {

        if (Utils.isOnline(this)) {
            val modelsignup = ModelSingup()

            modelsignup.signup = ConstantKey.KEY_REGISTER_ID
            modelsignup.name = editTextNewName.text.toString()
            modelsignup.email = editTextNewEmail.text.toString()
            modelsignup.mobile = editTextMobile.text.toString()
            modelsignup.password = editTextNewPassword.text.toString()
            modelsignup.birth_date = editTextDob.text.toString()
            modelsignup.gender = gender
            ApiInterface.doSignup(this, modelsignup, this)
        } else {
            Toast.makeText(this, "No Internet Connection", Toast.LENGTH_SHORT).show()
        }

    }

    override fun getsignup(jsonObject: JSONObject) {

        var message = jsonObject.getString("message")
        var success = jsonObject.getString("success")
        if (success.equals("true")) {
            Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
            startActivity(Intent(this, MainActivity::class.java))
        } else if (success.equals("false")){
            Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
        }

    }


}


