package com.bettersolver.activity

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.bettersolver.ApiInterface
import com.bettersolver.ConstantKey
import com.bettersolver.R
import com.bettersolver.interfaces.ForgotPasswordCallback
import com.bettersolver.model.ModelForgotPassword
import com.bettersolver.utils.Utils
import kotlinx.android.synthetic.main.activity_forgot_password.*
import org.json.JSONObject

class ForgotPasswordActivity : AppCompatActivity(), ForgotPasswordCallback{

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_forgot_password)

        initAction()

    }

    private fun initAction() {
        imgBack.setOnClickListener {
            startActivity(Intent(this@ForgotPasswordActivity, LoginActivity::class.java))
            finish()

        }
        cirForgotButton.setOnClickListener {
            //here we will sent password to the user registered mailid to server
            sendtomail()
        }
    }

    private fun sendtomail(){
        if (Utils.isOnline(this)) {
            val modelforgotpassword = ModelForgotPassword()

            modelforgotpassword.forgotpassword= ConstantKey.KEY_FORGOTPASS
            modelforgotpassword.email = editTextEmail.text.toString()
            ApiInterface.doForgotPassword(this, modelforgotpassword, this)
        } else {
            Toast.makeText(this, "Please enter registered email", Toast.LENGTH_SHORT).show()
        }
    }

    override fun getsendtomail(jsonObject: JSONObject) {

        var message = jsonObject.getString("message")
        var success = jsonObject.getString("success")
        if (success.equals("true")) {
            Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
            startActivity(Intent(this, LoginActivity::class.java))
        } else if (success.equals("false")){
            Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
        }

    }

}