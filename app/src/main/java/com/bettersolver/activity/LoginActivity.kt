package com.bettersolver.activity

import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.os.AsyncTask
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.bettersolver.ApiInterface
import com.bettersolver.ConstantKey
import com.bettersolver.R
import com.bettersolver.interfaces.LoginCallback
import com.bettersolver.model.ModelLogin
import com.bettersolver.utils.SharedPref
import com.bettersolver.utils.Utils
import com.bettersolver.utils.VolleySingleton
import kotlinx.android.synthetic.main.activity_login.*
import org.json.JSONException
import org.json.JSONObject
import java.util.*

class LoginActivity : AppCompatActivity() {

    lateinit var context: Context
    val loginURL = "http://demo.bettersolver.com/app_api.php?do=signin"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        context = this
        initAction()
    }

    private fun initAction() {
        cirLoginButton.setOnClickListener {
          //  startActivity(Intent(this@LoginActivity, MainActivity::class.java))
            if (Utils.isOnline(context)) {
                validateUserData()
                //NetworkTask(this).execute()
            } else {
                Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show()
            }
        }

        txtRegister.setOnClickListener {
            startActivity(Intent(this@LoginActivity, RegisterActivity::class.java))
            finish()
        }

        ivAddRegister.setOnClickListener {
            startActivity(Intent(this@LoginActivity, RegisterActivity::class.java))
            finish()
        }

        tvForgotpassword.setOnClickListener {
            startActivity(Intent(this@LoginActivity, ForgotPasswordActivity::class.java))
            finish()
        }

    }

    class NetworkTask(var activity: LoginActivity) : AsyncTask<Void, Void, Void>(){

        var dialog = Dialog(activity,android.R.style.Theme_Translucent_NoTitleBar)

        override fun onPreExecute() {
            val view = activity.layoutInflater.inflate(R.layout.full_screen_progressbar,null)
            dialog.setContentView(view)
            dialog.setCancelable(false)
            dialog.show()
            super.onPreExecute()
        }

        override fun doInBackground(vararg p0: Void?): Void? {
            Thread.sleep(2000)
            return null
        }

        /*override fun onPostExecute(result: Void?) {
            super.onPostExecute(result)
            dialog.dismiss()
        }*/

    }

    private fun DoLogin() {
        if (Utils.isOnline(context)!!) {
            val modelLogin = ModelLogin()

            modelLogin.signin = ConstantKey.KEY_LOGIN_ID
            modelLogin.email = etEmail.text.toString()
            modelLogin.password = etPassword.text.toString()
            //ApiInterface.doLogin(context, modelLogin , this)

        } else {
            Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show()
        }
    }

/*
    override fun getlogin(jsonObject: JSONObject) {

        var jsonObjData = JSONObject()
        //Log.e("data", jsonObject.toString());
        if(jsonObject.has("success") && jsonObject.optBoolean("success")==true){
            Toast.makeText(context,"Login successfully",Toast.LENGTH_SHORT).show()
            val modelLogin = ModelLogin.ModelLoginData()
            jsonObjData = jsonObject.getJSONObject("data")

            modelLogin.userid = jsonObjData.getString("userid")
            Log.e("userid", modelLogin.userid);
            Log.e("fullname", jsonObjData.getString("fullname"));
            Log.e("emailid", jsonObjData.getString("emailid"));

            val intent = Intent(this, MainActivity::class.java)

            intent.putExtra("user_id",modelLogin.userid)
            this.startActivity(intent)
        }else{
            Toast.makeText(context,"Something wrong,Please try again",Toast.LENGTH_SHORT).show()
        }
    }
*/

        private fun validateUserData() { //first getting the values
            val modelLogin = ModelLogin()
            val email: String = etEmail.text.toString()
            val password: String = etPassword.text.toString()
            //checking if email is empty
            if (TextUtils.isEmpty(email)) {
                etEmail.error = "Please enter your Email Address"
                etEmail.requestFocus()
                // Vibrate for 100 milliseconds
                //  v.vibrate(100);
                cirLoginButton.isEnabled = true
                return
            }
            //checking if password is empty
            if (TextUtils.isEmpty(password)) {
                etPassword.error = "Please enter your password"
                etPassword.requestFocus()
                //Vibrate for 100 milliseconds
                 //  v.vibrate(100);
                cirLoginButton.setEnabled(true)
                return
            }

            //Login User if everything is fine
            loginUser()
            NetworkTask(this).execute()
        }

        private fun loginUser() { //first getting the values
            val email: String = etEmail.text.toString()
            val password: String = etPassword.text.toString()
            //Call our volley library
            var obj = JSONObject()
            val stringRequest: StringRequest =
                object : StringRequest(Method.POST, loginURL,
                    Response.Listener { response ->
                        try {
                            obj = JSONObject(response)

                            var dataobj = JSONObject()
                            dataobj =  obj.getJSONObject("data")
                            var user_id = dataobj.getString("userid")

                            Log.e("data",obj.toString())

                            if ((obj.has("success") && obj.optBoolean("success")==false)) {
                                //Toast.makeText(applicationContext, obj.getString("message"), Toast.LENGTH_SHORT).show()
                                Toast.makeText(context,"Something wrong,Please try again",Toast.LENGTH_SHORT).show()
                            }
                            else if((obj.has("success") && obj.optBoolean("success")==true)) {
                               // Toast.makeText(applicationContext, obj.getString("message"), Toast.LENGTH_SHORT).show()
                                Toast.makeText(context,"Login successfully",Toast.LENGTH_SHORT).show()
                                SharedPref.getInstance(applicationContext).storeUserEmail("user_id")

                                Utils.setPref(context, "user_id", user_id)
                                finish()
                                startActivity(Intent(applicationContext, MainActivity::class.java))
                            }
                        } catch (e: JSONException) {
                            e.printStackTrace()
                        }
                    },
                    Response.ErrorListener { error ->
                        Toast.makeText(
                            applicationContext,
                            "Connection Error$error",
                            Toast.LENGTH_SHORT
                        ).show()
                        error.printStackTrace()
                    }) {
                    override fun getParams(): Map<String, String> {
                        val params: MutableMap<String, String> =
                            HashMap()
                        params["email"] = email
                        params["password"] = password

                        return params
                    }
                }
            VolleySingleton.getInstance(this@LoginActivity).addToRequestQueue(stringRequest)
        }

    override fun onBackPressed() {
        finishAffinity()
    }
}
