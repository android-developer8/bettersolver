package com.bettersolver.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.bettersolver.R;

import br.com.simplepass.loading_button_lib.customViews.CircularProgressButton;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class Login extends AppCompatActivity {
    @BindView(R.id.etEmail)
    EditText etEmail;
    @BindView(R.id.etPassword)
    EditText etPassword;
    @BindView(R.id.tvForgotpassword)
    TextView tvForgotpassword;
    @BindView(R.id.cirLoginButton)
    CircularProgressButton cirLoginButton;
    @BindView(R.id.txtRegister)
    TextView txtRegister;
    @BindView(R.id.img_fb)
    ImageView imgFb;
    @BindView(R.id.img_google)
    ImageView imgGoogle;
    @BindView(R.id.ivAddRegister)
    ImageView ivAddRegister;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);


    }

    @OnClick({R.id.tvForgotpassword, R.id.cirLoginButton, R.id.txtRegister, R.id.img_fb, R.id.img_google, R.id.ivAddRegister})
    public void onClick(View view) {
        Intent intent=null;
        switch (view.getId()) {
            case R.id.tvForgotpassword:
                intent=new Intent(Login.this,ForgotPasswordActivity.class);
                startActivity(intent);
                finish();
                break;
            case R.id.cirLoginButton:
//                if (Utils.isOnline(Login.this)) {
////                    validateUserData()
//                    //NetworkTask(this).execute()
//                } else {
//                    Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
//                }
                break;
            case R.id.txtRegister:
                intent=new Intent(Login.this,RegisterActivity.class);
                startActivity(intent);
                finish();
                break;
            case R.id.img_fb:

                //facebook login

                break;
            case R.id.img_google:
                //google plus login
                break;
            case R.id.ivAddRegister:
                intent=new Intent(Login.this,RegisterActivity.class);
                startActivity(intent);
                finish();
                break;
        }
    }
}
