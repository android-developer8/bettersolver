package com.bettersolver.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.bettersolver.R
import kotlinx.android.synthetic.main.activity_edit_profile.*

class EditProfile : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_profile)

        initAction()
    }

    private fun initAction() {
        ivCancel.setOnClickListener {
            finish()
        }
    }
}
