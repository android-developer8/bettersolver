package com.bettersolver.activity

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.bettersolver.ApiInterface
import com.bettersolver.ConstantKey
import com.bettersolver.R
import com.bettersolver.interfaces.ChangePasswordCallback
import com.bettersolver.model.ModelChangePassword
import com.bettersolver.model.ModelForgotPassword
import com.bettersolver.utils.Utils
import kotlinx.android.synthetic.main.activity_forgot_password.*
import kotlinx.android.synthetic.main.activity_forgot_password.imgBack
import kotlinx.android.synthetic.main.change_password.*
import org.json.JSONObject

class ChangePasswordActivity : AppCompatActivity(), ChangePasswordCallback {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.change_password)
        initAction()
    }

    private fun initAction(){

        imgBack.setOnClickListener {
            finish()
        }

        cirChangeButton.setOnClickListener {
            //here we will change pass of user to server
            changeuserpass()
        }
    }

    private fun changeuserpass(){
        if (Utils.isOnline(this)) {
            val modelChangePassword = ModelChangePassword()

            modelChangePassword.email = etEmail.text.toString()
            modelChangePassword.password = etNewPassword.text.toString()
            modelChangePassword.changepassword= ConstantKey.KEY_CHANGEPASS
            ApiInterface.doChangePassword(this, modelChangePassword, this)
        } else {
            Toast.makeText(this, "Please enter registered email", Toast.LENGTH_SHORT).show()
        }
    }

    override fun getnewpassword(jsonObject: JSONObject) {
        var message = jsonObject.getString("message")
        var success = jsonObject.getString("success")
        if (success.equals("true")) {
            Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
        } else if (success.equals("false")){
            Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
            startActivity(Intent(this, LoginActivity::class.java))
        }
    }





}