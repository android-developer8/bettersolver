package com.bettersolver.activity

import android.app.Activity
import android.content.Context
import android.os.Build
import android.os.Bundle
import android.view.View
import android.view.WindowManager
import android.widget.Toast
import androidx.annotation.ColorRes
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bettersolver.ApiInterface
import com.bettersolver.R
import com.bettersolver.adapter.PostCommentAdapter
import com.bettersolver.interfaces.GetPostCommentCallback
import com.bettersolver.interfaces.OnItemClickCallback
import com.bettersolver.model.ModelGetComment
import com.bettersolver.model.ModelLogin
import com.bettersolver.model.ModelPostComment
import com.bettersolver.model.PostWallHome
import com.bettersolver.utils.Utils
import kotlinx.android.synthetic.main.activity_post_comment.*
import okhttp3.internal.Util

class PostCommentActivity : AppCompatActivity(),GetPostCommentCallback, OnItemClickCallback {

    lateinit var context: Context
    private var arrOfPostComment = ArrayList<ModelGetComment>()
    private lateinit var postcommentAdapter: PostCommentAdapter
    var post_id = ""
    var user_id = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_post_comment)
        context = this
        post_id = intent.getStringExtra("post_id")
        val modelLogin = ModelLogin.ModelLoginData()
        user_id = Utils.getPref(this,"user_id", 1.toString())
        //user_id = intent.getStringExtra("user_id")
        initAction()
        setSystemBarColor(this, android.R.color.black)
        getData()

    }

    private fun initAction() {

        imgBack.setOnClickListener {
            finish()
        }
        fab_sendcomment.setOnClickListener {
            addComment()
        }
    }

    private fun getData(){
        rvgetcomment.visibility = View.GONE
        val modelpostwallhome = PostWallHome()
        modelpostwallhome.id = post_id
        ApiInterface.getpostcomment(context,modelpostwallhome,this)
    }

    private fun addComment() {
        if (et_add_comment.text.isEmpty()){
            Toast.makeText(context, "Enter comment", Toast.LENGTH_SHORT).show()
        } else{
            val modelPostComment = ModelPostComment()
            val modelpostwallhome = PostWallHome()
            val modelLogin = ModelLogin.ModelLoginData()
            modelpostwallhome.id = post_id
            modelLogin.userid = user_id
            modelPostComment.comment = et_add_comment.text.toString()
            ApiInterface.sendComment(context, modelPostComment, modelLogin, modelpostwallhome, this)
            getData()
        }
    }

   /* override fun sendPostComments(jsonObject: JSONObject) {
        val message = jsonObject.getString("message")
        val success = jsonObject.getString("success")
        if (success.equals("true")) {
            Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
            startActivity(Intent(this, PostCommentActivity::class.java))
//            val modelLogin = ModelLogin.ModelLoginData()
//            intent.putExtra("user_id",modelLogin.userid)

        } else if (success.equals("false")){
            Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
        }
    }*/



    override fun getAllPostComments(arrOfPostComment: ArrayList<ModelGetComment>) {
        // lottieLoader.visibility = View.GONE
        rvgetcomment.visibility = View.VISIBLE
        this.arrOfPostComment = arrOfPostComment
        setRvAdapter()
    }

    private fun setRvAdapter() {
        val layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
        rvgetcomment.layoutManager = layoutManager
        postcommentAdapter = PostCommentAdapter(context, arrOfPostComment, this)
        rvgetcomment.adapter = postcommentAdapter

    }


    override fun onClickItem(view: View, position: Int) {

    }

    private fun setSystemBarColor(act: Activity, @ColorRes color: Int) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            val window = act.window
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
            window.statusBarColor = act.resources.getColor(color)
        }
    }
}
