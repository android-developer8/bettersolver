package com.bettersolver.activity;

import android.os.Bundle;
import android.os.PersistableBundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.bettersolver.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ChangePassword extends AppCompatActivity {
    @BindView(R.id.etEmail)
    EditText etEmail;
    @BindView(R.id.etNewPassword)
    EditText etNewPassword;
    @BindView(R.id.cirChangeButton)
    Button cirChangeButton;
    @BindView(R.id.imgBack)
    ImageView imgBack;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState, @Nullable PersistableBundle persistentState) {
        super.onCreate(savedInstanceState, persistentState);
        setContentView(R.layout.change_password);
        ButterKnife.bind(this);
    }


    @OnClick({R.id.cirChangeButton, R.id.imgBack})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.cirChangeButton:
//                changeuserpass()
                break;
            case R.id.imgBack:
//                finish();
                break;
        }
    }
}
