package com.bettersolver.model

import java.io.Serializable

class ModelPostComment : Serializable {

    var postid =""
    var userid =""
    var comment =""

}