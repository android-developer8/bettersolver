package com.bettersolver.model

import java.io.Serializable

class Category: Serializable{
    var id=""
    var category_name=""
    var category_image=""
}