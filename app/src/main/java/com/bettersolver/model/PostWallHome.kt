package com.bettersolver.model

import java.io.Serializable
import java.util.ArrayList

class PostWallHome:Serializable{

    var id=""
    var rec_date=""
    var update_date=""
    var type=""
    var userid=""
    var post_title=""
    var post_content=""
    var anonymously=""
    var post_for=""
    var followers=""
    var views=""
    var thanks=""
    var viewpoint=""
    var isActive=""
    var isDelete=""
    var full_name=""
    var profileimage=""
    var username=""
    var post_link=""
    var post_video_id=""
    var post_image=""
    var category=""
}

