package com.bettersolver.model

import java.io.Serializable

class ModelGetProfile : Serializable{

    var id = ""
    var rec_date = ""
    var full_name = ""
    var emailid = ""
    var username = ""
    var password  = ""
    var mobile  = ""
    var otp  = ""
    var potp  = ""
    var token  = ""
    var token_status = ""
    var isverify = ""
    var birth_date = ""
    var gender = ""
    var profileimage = ""
    var profile_status = ""
    var description = ""
    var occupations = ""
    var fb_link = ""
    var instagram_link = ""
    var website_link = ""
    var place = ""
    var followers = ""
    var following = ""
    var isActive = ""
    var isDelete = ""
    var reg_complete = ""
}

    /*
    "data": {
        "id": "31",
        "rec_date": "2020-03-27 14:00:26",
        "full_name": "pranay",
        "emailid": "pranay@gmail.com",
        "username": "pranay",
        "password": "3495b60daadffb06d1de5eb5f607337f",
        "mobile": "",
        "otp": "0",
        "potp": "0",
        "token": null,
        "token_status": "0",
        "isverify": "0",
        "birth_date": "1970-01-01",
        "gender": "0",
        "profileimage": "user-placeholder.jpg",
        "profile_status": "",
        "description": "",
        "occupations": "",
        "fb_link": "",
        "instagram_link": "",
        "website_link": "",
        "place": "surat",
        "followers": "2",
        "following": "0",
        "isActive": "1",
        "isDelete": "0",
        "reg_complete": "0"
    }*/
