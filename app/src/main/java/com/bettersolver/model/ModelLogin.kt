package com.bettersolver.model

import java.io.Serializable
import kotlin.collections.ArrayList

class ModelLogin:Serializable{
    var signin =""
    var email =""
    var password =""
    var data = ArrayList<ModelLoginData>()

    class ModelLoginData:Serializable{
        var userid = ""
        var fullname = ""
        var emailid =""
    }
}


