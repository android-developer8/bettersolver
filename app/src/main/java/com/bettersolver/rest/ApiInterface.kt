@file:Suppress("NAME_SHADOWING")

package com.bettersolver

import android.content.Context
import android.util.Log
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.bettersolver.fragments.CategoryFragment
import com.bettersolver.fragments.HomeFragment
import com.bettersolver.interfaces.*
import com.bettersolver.model.*
import com.bettersolver.utils.VolleySingleton
import org.json.JSONException
import org.json.JSONObject

object ApiInterface {

    fun doSignup(context: Context, modelsignup: ModelSingup, singupCallback: SingupCallback) {
            var jsonObject = JSONObject()
            val jsonStringRequest = object : StringRequest(Method.POST, ConstantValue.BASEURL,
                Response.Listener { response ->
                    jsonObject = JSONObject(response)
                    Log.e("data1", jsonObject.toString());
                    singupCallback.getsignup(jsonObject)
                },
                Response.ErrorListener { error ->
                    singupCallback.getsignup(jsonObject)
                }) {
                override fun getParams(): MutableMap<String, String> {
                    val params = HashMap<String, String>()
                    params["name"] = modelsignup.name
                    params["email"] = modelsignup.email
                    params["mobile"] = modelsignup.mobile
                    params["password"] = modelsignup.password
                    params["gender"] = modelsignup.gender
                    params["birth_date"] = modelsignup.birth_date
                    params["do"] = modelsignup.signup
                    return params
                }
            }
            VolleySingleton.getInstance(context).addToRequestQueue(jsonStringRequest)
        }

    fun doLogin(context: Context, modelLogin: ModelLogin, loginCallback: LoginCallback) {
        val arrOfModelLogin = ArrayList<ModelLogin.ModelLoginData>()
        var jsonObject = JSONObject()
        val jsonStringRequest = object : StringRequest(Method.POST, ConstantValue.BASEURL,
            Response.Listener { response ->
                jsonObject = JSONObject(response)
                Log.e("data", jsonObject.toString());
                loginCallback.getlogin(jsonObject)

            },
            Response.ErrorListener { error ->
                loginCallback.getlogin(jsonObject)

            }) {
            override fun getParams(): MutableMap<String, String> {
                val param = HashMap<String, String>()
                param["do"] = modelLogin.signin
                param["email"] = modelLogin.email
                param["password"] = modelLogin.password
                return param
            }
        }
        VolleySingleton.getInstance(context).addToRequestQueue(jsonStringRequest)
    }

    fun doForgotPassword(context: Context, modelforgotpassword: ModelForgotPassword, forgotPasswordCallback: ForgotPasswordCallback) {
        var jsonObject = JSONObject()
        val jsonStringRequest = object : StringRequest(Method.POST, ConstantValue.BASEURL,
            Response.Listener { response ->
                jsonObject = JSONObject(response)
                Log.e("data1", jsonObject.toString());
                forgotPasswordCallback.getsendtomail(jsonObject)
            },
            Response.ErrorListener { error ->
                forgotPasswordCallback.getsendtomail(jsonObject)
            }) {
            override fun getParams(): MutableMap<String, String> {
                val param = HashMap<String, String>()

                param["email"] = modelforgotpassword.email
                param["do"] = modelforgotpassword.forgotpassword
                return param
            }
        }
        VolleySingleton.getInstance(context).addToRequestQueue(jsonStringRequest)
    }

    fun doChangePassword(context: Context, modelChangePassword: ModelChangePassword, changePasswordCallback: ChangePasswordCallback) {
        var jsonObject = JSONObject()
        val jsonStringRequest = object : StringRequest(Method.POST, ConstantValue.BASEURL,
            Response.Listener { response ->
                jsonObject = JSONObject(response)
                Log.e("data1", jsonObject.toString());
                changePasswordCallback.getnewpassword(jsonObject)
            },
            Response.ErrorListener { error ->
                changePasswordCallback.getnewpassword(jsonObject)
            }) {
            override fun getParams(): MutableMap<String, String> {
                val param = HashMap<String, String>()
                param["email"] = modelChangePassword.email
                param["password"] = modelChangePassword.password
                param["do"] = modelChangePassword.changepassword
                return param
            }
        }
        VolleySingleton.getInstance(context).addToRequestQueue(jsonStringRequest)
    }



    fun getpostwall(context: Context, getPostWallCallback: HomeFragment) {
        val arrOfPostWall = ArrayList<PostWallHome>()
        val jsonStringRequest = object : StringRequest(Request.Method.POST, ConstantValue.BASEURL,
            Response.Listener { response ->
                val jsonObject = JSONObject(response)
                Log.e("home df =======",response)
                try {
                    val postwallhome = jsonObject.getJSONArray("data")

                    for (i in 0 until postwallhome.length()) {
                        val jsonObjectContact = postwallhome.getJSONObject(i)

                        val postwallhome = PostWallHome()
                        postwallhome.id = jsonObjectContact.getString("id" )
                        postwallhome.userid = jsonObjectContact.getString("userid")
                        postwallhome.username = jsonObjectContact.getString("username")
                        postwallhome.category = jsonObjectContact.getString("category")
                        postwallhome.post_title = jsonObjectContact.getString("post_title")
                        postwallhome.post_content = jsonObjectContact.getString("post_content")
                        postwallhome.thanks = jsonObjectContact.getString("thanks")
                        postwallhome.viewpoint = jsonObjectContact.getString("viewpoint")
                        postwallhome.views = jsonObjectContact.getString("views")
                        postwallhome.followers = jsonObjectContact.getString("followers")

                        postwallhome.type = jsonObjectContact.getString("type")

                        arrOfPostWall.add(postwallhome)

                    }

                    getPostWallCallback.getAllPostWall(arrOfPostWall)
                } catch (e: JSONException) {
                    e.printStackTrace()
                }

            },
            Response.ErrorListener { error ->
                Log.d("Error", error.message)
                getPostWallCallback.getAllPostWall(arrOfPostWall)
            }) {
            override fun getParams(): MutableMap<String, String> {
                val param = HashMap<String, String>()
                param[ConstantKey.KEY_DO] = "getpost"
                Log.e("login id", ConstantKey.KEY_DO);
                return param
            }
        }
        VolleySingleton.getInstance(context).addToRequestQueue(jsonStringRequest)
    }

    fun getpostcomment(context: Context, modelpostwallhome: PostWallHome, getPostCommentCallback: GetPostCommentCallback) {
        val arrOfPostComment = ArrayList<ModelGetComment>()
        val jsonStringRequest = object : StringRequest(Request.Method.POST, ConstantValue.BASEURL,
            Response.Listener { response ->

                val jsonObject = JSONObject(response)
                Log.e("post id",modelpostwallhome.id)
                Log.e("df================",response)
                try {
                    val jsonArrayGetComment = jsonObject.getJSONArray("data")
                    for (i in 0 until jsonArrayGetComment.length()) {
                        val jsonObjectComment = jsonArrayGetComment.getJSONObject(i)
                        val modalGetComment = ModelGetComment()
                        modalGetComment.id = jsonObjectComment.getString("id")
                        modalGetComment.username = jsonObjectComment.getString("username")
                        modalGetComment.comment = jsonObjectComment.getString("comment")

                        arrOfPostComment.add(modalGetComment)
                    }

                    getPostCommentCallback.getAllPostComments(arrOfPostComment)
                } catch (e: JSONException) {
                    e.printStackTrace()
                }

            },
            Response.ErrorListener { error ->
                Log.d("Error", error.message)
                getPostCommentCallback.getAllPostComments(arrOfPostComment)
            }) {
            override fun getParams(): MutableMap<String, String>{
                val param = HashMap<String, String>()
                param[ConstantKey.KEY_DO] = "getcomment"
                param["postid"] = modelpostwallhome.id

                return param
            }
        }
        VolleySingleton.getInstance(context).addToRequestQueue(jsonStringRequest)
    }

    fun sendComment(context: Context, modelPostComment: ModelPostComment, modelLogin: ModelLogin.ModelLoginData, modelpostwallhome: PostWallHome, getPostCommentCallback: GetPostCommentCallback) {
        var jsonObject = JSONObject()
        val jsonStringRequest = object : StringRequest(Method.POST, ConstantValue.BASEURL,
            Response.Listener { response ->
                Log.e("user..id", modelLogin.userid)
                Log.e("post...id", modelpostwallhome.id)
                Log.e("commnt.......", modelPostComment.comment)
                try {
                    jsonObject = JSONObject(response)
                    getPostCommentCallback.sendPostComments(jsonObject)
                } catch (e: JSONException) {
                    getPostCommentCallback.sendPostComments(jsonObject)
                }
            },
            Response.ErrorListener { error ->
                getPostCommentCallback.sendPostComments(jsonObject)
            }) {
            override fun getParams(): MutableMap<String, String> {
                val param = HashMap<String, String>()
                param[ConstantKey.KEY_DO] = "postcomment"
                param["postid"] = modelpostwallhome.id
                param["userid"] = modelLogin.userid
                param["comment"] = modelPostComment.comment

                return param
            }
        }
        VolleySingleton.getInstance(context).addToRequestQueue(jsonStringRequest)
    }



    fun getcategory(context: Context, getCategoryCallback: CategoryFragment) {
        val arrOfCategry = ArrayList<Category>()
        val jsonStringRequest = object : StringRequest(Request.Method.POST, ConstantValue.BASEURL,
            Response.Listener { response ->
                val jsonObject = JSONObject(response)
                try {
                    val category = jsonObject.getJSONArray("data")

                    for (i in 0 until category.length()) {
                        val jsonObjectContact = category.getJSONObject(i)

                        val category = Category()
                        category.id = jsonObjectContact.getString("id")
                        category.category_name = jsonObjectContact.getString("category_name")
                        category.category_image = jsonObjectContact.getString("category_image")

                        arrOfCategry.add(category)

                    }

                    getCategoryCallback.getAllCategory(arrOfCategry)
                } catch (e: JSONException) {
                    e.printStackTrace()
                }

            },
            Response.ErrorListener { error ->
                Log.d("Error", error.message)
                getCategoryCallback.getAllCategory(arrOfCategry)
            }) {
            override fun getParams(): MutableMap<String, String> {
                val param = HashMap<String, String>()
                param[ConstantKey.KEY_DO] = "getcategory"
                Log.e("login id", ConstantKey.KEY_DO);
                return param
            }
        }
        VolleySingleton.getInstance(context).addToRequestQueue(jsonStringRequest)
    }


}







