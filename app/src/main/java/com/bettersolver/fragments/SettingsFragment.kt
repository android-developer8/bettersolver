package com.bettersolver.fragments


import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.bettersolver.R
import com.bettersolver.activity.ChangePasswordActivity
import kotlinx.android.synthetic.main.fragment_settings.*


class SettingsFragment : Fragment() {

    private lateinit var viewSetting: View
    var mCtx: Context? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?): View? {
        viewSetting =inflater.inflate(R.layout.fragment_settings, container, false)

        return viewSetting
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        lyt_change_password.setOnClickListener {
            val intent = Intent(context!!, ChangePasswordActivity::class.java)
            startActivity(intent)
        }

    }



}
