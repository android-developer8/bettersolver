package com.bettersolver.fragments


import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.navigation.NavController
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bettersolver.ApiInterface
import com.bettersolver.R
import com.bettersolver.activity.AskAQuestionActivity
import com.bettersolver.activity.MainActivity
import com.bettersolver.activity.PollActivity
import com.bettersolver.activity.PostActivity
import com.bettersolver.adapter.PostWallAdapter
import com.bettersolver.interfaces.GetPostWallCallback
import com.bettersolver.interfaces.OnItemClickCallback
import com.bettersolver.model.PostWallHome
import com.bettersolver.utils.Utils
import kotlinx.android.synthetic.main.fragment_home.*


class HomeFragment : Fragment() , GetPostWallCallback, OnItemClickCallback { //OnClickListener

    lateinit var viewPostWall: View
    private var arrOfPostWall = ArrayList<PostWallHome>()
    private lateinit var rvPostWallList: RecyclerView
    private lateinit var postwallAdapter: PostWallAdapter



    private lateinit var navController: NavController
    var isOpen = false
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        viewPostWall = inflater.inflate(R.layout.fragment_home, container, false)

        initDefine()
        (activity as MainActivity).supportActionBar?.title = getString(R.string.title_home_fragment )
        return viewPostWall
    }

    override fun getAllPostWall(arrOfPostWall: ArrayList<PostWallHome>) {
        // lottieLoader.visibility = View.GONE
        // rvCategoryList.visibility=View.VISIBLE
        this.arrOfPostWall = arrOfPostWall
        setRvAdapter()
    }

    private fun setRvAdapter() {
        val layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
        rvPostWallList.layoutManager = layoutManager
        postwallAdapter = PostWallAdapter(arrOfPostWall)
        rvPostWallList.adapter = postwallAdapter

    }

    private fun initDefine() {
        rvPostWallList = viewPostWall.findViewById(R.id.rvPostWallList)
        if (Utils.isOnline(context)!!) {
            ApiInterface.getpostwall(context!!,this)
        } else {
            Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show()
        }
    }

    override fun onClickItem(view: View, position: Int) {

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        navController = findNavController() //Initialising navController


        postfab.setOnClickListener {
            val intent = Intent(context!!, PostActivity::class.java)
            startActivity(intent)
        }

        askfab.setOnClickListener {
            val intent = Intent(context!!, AskAQuestionActivity::class.java)
            startActivity(intent)
        }

        pollfab.setOnClickListener {
            val intent = Intent(context!!, PollActivity::class.java)
            startActivity(intent)
        }

        val fabOpen = AnimationUtils.loadAnimation(context, R.anim.fab_open)
        val fabClose = AnimationUtils.loadAnimation(context,R.anim.fab_close)
        val fabRClockwise = AnimationUtils.loadAnimation(context,R.anim.rotate_clockwise)
        val fabAntiClockwise = AnimationUtils.loadAnimation(context,R.anim.rotate_anticlockwise)

        fab.setOnClickListener {

            if (isOpen){

                lyt_postfab.visibility = View.GONE
                lyt_askquestionfab.visibility = View.GONE
                lyt_pollfab.visibility = View.GONE
                lyt_postfab.startAnimation(fabClose)
                lyt_askquestionfab.startAnimation(fabClose)
                lyt_pollfab.startAnimation(fabClose)
                fab.startAnimation(fabRClockwise)

                isOpen = false
            }

            else {

                lyt_postfab.visibility = View.VISIBLE
                lyt_askquestionfab.visibility = View.VISIBLE
                lyt_pollfab.visibility = View.VISIBLE
                lyt_postfab.startAnimation(fabOpen)
                lyt_askquestionfab.startAnimation(fabOpen)
                lyt_pollfab.startAnimation(fabOpen)
                fab.startAnimation(fabAntiClockwise)

                postfab.isClickable
                val clickable = askfab.isClickable

                isOpen = true
            }


        }
    }





}


