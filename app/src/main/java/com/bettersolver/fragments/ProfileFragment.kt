package com.bettersolver.fragments


import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bettersolver.R
import com.bettersolver.activity.EditProfile
import kotlinx.android.synthetic.main.fragment_profile.*


class ProfileFragment : Fragment() {

    lateinit var viewEditProfile: View

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        viewEditProfile = inflater.inflate(R.layout.fragment_profile, container, false)

        return viewEditProfile
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        tv_editprofile.setOnClickListener {
            val intent = Intent(context!!, EditProfile::class.java)
            startActivity(intent)
        }

    }

}
