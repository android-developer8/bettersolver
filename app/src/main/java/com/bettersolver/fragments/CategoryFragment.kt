package com.bettersolver.fragments


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

import com.bettersolver.ApiInterface
import com.bettersolver.R
import com.bettersolver.adapter.CategoryAdapter
import com.bettersolver.interfaces.GetCategoryCallback
import com.bettersolver.interfaces.OnItemClickCallback
import com.bettersolver.model.Category

import com.bettersolver.utils.Utils


class CategoryFragment : Fragment(), GetCategoryCallback, OnItemClickCallback {

    lateinit var viewCategory: View
    private var arrOfCategory = ArrayList<Category>()
    private lateinit var rvCategoryList: RecyclerView
    private lateinit var categoryAdapter: CategoryAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?): View? {
        viewCategory = inflater.inflate(R.layout.fragment_category, container, false)
        initDefine()
        return viewCategory
    }

    override fun getAllCategory(arrOfCategory: ArrayList<Category>) {
       // lottieLoader.visibility = View.GONE
       // rvCategoryList.visibility=View.VISIBLE
        this.arrOfCategory = arrOfCategory
        setRvAdapter()
    }

    private fun setRvAdapter() {
        val layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
        rvCategoryList.layoutManager = layoutManager
        categoryAdapter = CategoryAdapter(context!!, arrOfCategory, this)
        rvCategoryList.adapter = categoryAdapter
    }

    private fun initDefine() {
        rvCategoryList = viewCategory.findViewById(R.id.rvCategoryList)
        if (Utils.isOnline(context)!!) {
            ApiInterface.getcategory(context!!,this)
        } else {
            Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show()
        }
    }

    override fun onClickItem(view: View, position: Int) {

    }


}
