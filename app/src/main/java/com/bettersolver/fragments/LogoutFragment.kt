package com.bettersolver.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.bettersolver.R
import com.bettersolver.utils.SharedPref
import com.bettersolver.utils.SharedPref.mCtx


class LogoutFragment : Fragment() {

    lateinit var viewLogout: View


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?): View? {
        //viewLogout =inflater.inflate(R.layout.fragment_logout, container, false)
        SharedPref.getInstance(activity).logout()

        return viewLogout
    }


}